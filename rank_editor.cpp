/*
 * This entire thing is very fragile and dependent on `order` being 2 bytes long, `exp` 4 bytes long, and `level` 2 bytes long. You have to pray that your compiler interprets `short` and `int` as such, or else you're gonna have to figure out which data types to use
 * Made by Autumn Dawn, if that matters
*/
#include <iostream>
#include <string>
#include <cstdio>
#include <vector>
#include <cstring>

#define CLASS_AMOUNT 10

const char HEADER[] = "Horde_Rank"; // I'm not sure if this can change so I'm assuming it's always the same. If not, change this to what the header is.
const std::vector<std::string> CLASS_NAME = 
{
	"Survivor",
	"Assault",
	"Heavy",
	"Medic",
	"Demolition",
	"Ghost",
	"Engineer",
	"Berserker",
	"Warden",
	"Cremator"
};
const char HELP[] = "Available commands:\nhelp - print this message\nshow - show currently set values\nset [ID] [level|exp] [value] - set given class' or subclass' level or exp to given value\nsave - save the data to file\nexit";

// For whatever reason subclasses save order as 32-bit unsigned integer, while classes save it as 16-bit signed integer
struct HClass
{
	short order;
	int exp;
	short level;
	std::string name;
};

struct HSubClass
{
	unsigned int order;
	int exp;
	short level;
};

namespace RankEditor
{
	std::vector<struct HClass> classes;
	std::vector<struct HSubClass> subclasses;
	
	bool running = true;
	// Both read and write functions return: 0 on success, 1 when file could not be opened
	int read_data(const char fname[]);
	int write_data(const char fname[]);
	void print_data();
	// Argument: vector containing parts of the instruction
	void interpret_command(std::vector<std::string> argv);
	// Returns 0 on successful value change, 1 on invalid ID, 2 on invalid mode
	int change_value(long target_order, std::string mode, int value);
};

// I should probably be using a library function for this.
std::vector<std::string> split_at_spaces(std::string s);


int main()
{
	std::string in_filename;
	
	// TODO I could probably rework this into a command
	while (in_filename.empty()) {
		int return_code;
		std::cout << "Write the name of file to edit:\n> ";
		std::cin >> in_filename;
		return_code = RankEditor::read_data(in_filename.c_str());
		if (return_code == 1) {
			std::cout << "File could not be opened.\n";
			in_filename.clear();
		} else if (return_code == 2) {
			std::cout << "Incorrect input file (wrong header).\n";
			in_filename.clear();
		}
	}
	
	std::cout << HELP << std::endl;
	while (RankEditor::running) {
		std::string command;
		std::cout << "> ";
		do { // Get rid of residual data from the stream
			std::getline(std::cin, command);
		} while (command == "");
		RankEditor::interpret_command(split_at_spaces(command));
	}
	return 0;
}

int RankEditor::read_data(const char fname[])
{
	const size_t HEADER_LENGTH = sizeof(HEADER)/sizeof(HEADER[0]);
	char header[HEADER_LENGTH];
	FILE* in_file;
	
	in_file = fopen(fname, "rb");
	if (in_file == nullptr)
		return 1;
	
	for (size_t i=0; i<HEADER_LENGTH-1; i++) // Exclude final character (supposed to be null)
		fread(&header[i], 1, 1, in_file);
	header[HEADER_LENGTH-1] = '\0';
	
	if (strcmp(header, HEADER)) {
		fclose(in_file);
		return 2;
	}
	
	
	// Begin proper reading
	for (unsigned long i=0; i<CLASS_AMOUNT; i++) {
		// Current class
		struct HClass cc;
		
		fread(&(cc.order), sizeof(cc.order), 1, in_file);
		fread(&(cc.exp), sizeof(cc.exp), 1, in_file);
		fread(&(cc.level), sizeof(cc.level), 1, in_file);
		
		cc.name = CLASS_NAME[cc.order];
		
		classes.emplace_back(cc);
	}
	
	// Read subclasses
	while (!feof(in_file)) {
		// Current subclass
		struct HSubClass cc;
		fread(&(cc.order), sizeof(cc.order), 1, in_file);
		fread(&(cc.exp), sizeof(cc.exp), 1, in_file);
		fread(&(cc.level), sizeof(cc.level), 1, in_file);
		
		subclasses.emplace_back(cc);
	}
	
	fclose(in_file);
	return 0;
}

int RankEditor::write_data(const char fname[])
{
	FILE* out_file;
	const size_t HEADER_LENGTH = sizeof(HEADER)/sizeof(HEADER[0]) - 1; // Won't be writing the null
	
	out_file = fopen(fname, "wb");
	if (out_file == nullptr)
		return 1;
	
	for (size_t i=0; i<HEADER_LENGTH; i++)
		fwrite(&HEADER[i], sizeof(HEADER[i]), 1, out_file);
	
	
	// Begin proper writing
	for (unsigned long i=0; i<CLASS_AMOUNT; i++) {
		// Current class
		struct HClass cc = classes[i];
		
		fwrite(&(cc.order), sizeof(cc.order), 1, out_file);
		fwrite(&(cc.exp), sizeof(cc.exp), 1, out_file);
		fwrite(&(cc.level), sizeof(cc.level), 1, out_file);
	}
	
	// Write subclasses
	size_t scsize = subclasses.size();
	for (size_t i=0; i<scsize; i++) {
		// Current subclass
		struct HSubClass cc = subclasses[i];
		fwrite(&(cc.order), sizeof(cc.order), 1, out_file);
		fwrite(&(cc.exp), sizeof(cc.exp), 1, out_file);
		fwrite(&(cc.level), sizeof(cc.level), 1, out_file);
	}
	
	fclose(out_file);
	return 0;
}

void RankEditor::print_data()
{
	std::cout << "Classes:\n";
	for (struct HClass cc : classes) {
		std::cout << "ID: " << cc.order << " (" << cc.name << "):\n\tlevel: " << cc.level << "\n\texp: " << cc.exp << "\n";
	}
	std::cout << "\nSubclasses:\n";
	for (struct HSubClass cc : subclasses) {
		std::cout << "ID: " << cc.order << ":\n\tlevel: " << cc.level << "\n\texp: " << cc.exp << "\n";
	}
	std::cout << std::endl;
}

// This could probably be cleaned up so that every command is its own function
void RankEditor::interpret_command(std::vector<std::string> argv)
{
	if (argv[0] == "")
		return;
	
	if (argv[0] == "help")
		std::cout << HELP << "\n";
	else if (argv[0] == "show")
		print_data();
	else if (argv[0] == "set") {
		int return_value;
		if (argv.size() != 4) {
			std::cout << "Improper number of arguments (required 3, given " << argv.size()-1 << ")\n";
			return;
		}
		return_value = change_value(std::stol(argv[1]), argv[2], std::stoi(argv[3]));
		if (return_value == 1)
			std::cout << "No class with this ID found.\n";
		else if (return_value == 2)
			std::cout << "Invalid mode.\n";
	} else if (argv[0] == "save") {
		std::string fname;
		std::cout << "Write the name of the output file (no spaces):\n> ";
		std::cin >> fname;
		if (write_data(fname.c_str()) == 1)
			std::cout << "File could not be opened.\n";
	} else if (argv[0] == "exit")
		running = false;
	else
		std::cout << "Command " << argv[0] << " not recognized.\n";
}

int RankEditor::change_value(const long target_order, const std::string mode, const int target_value)
{
	const size_t csize = classes.size();
	const size_t scsize = subclasses.size();
	
	for (size_t i=0; i<csize; i++) {
		if (classes[i].order == target_order) {
			if (mode == "level")
				classes[i].level = target_value;
			else if (mode == "exp")
				classes[i].exp = target_value;
			else
				return 2;
			std::cout << "Value changed.\n";
			return 0;
		}
	}
	for (size_t i=0; i<scsize; i++) {
		if (subclasses[i].order == target_order) {
			if (mode == "level")
				subclasses[i].level = target_value;
			else if (mode == "exp")
				subclasses[i].exp = target_value;
			else
				return 2;
			std::cout << "Value changed.\n";
			return 0;
		}
	}
	return 1;
}

std::vector<std::string> split_at_spaces(std::string s)
{
	size_t space = 0;
	std::vector<std::string> out;
	
	while (space != std::string::npos && space < s.size()) {
		std::string c = s.substr(space, s.find(" ", space)-space);
		if (c != " ") {
			out.push_back(c);
		}
		space = s.find(" ", space) + 1;
		if (space == 0)		// Because this idiotic piece of trash keeps breaking
			break;
	}
	if (out.empty())
		out = {""};
	return out;
}
